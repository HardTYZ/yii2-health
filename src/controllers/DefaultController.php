<?php

namespace hardtyz\health\controllers;

use yii\base\ErrorException;
use yii\web\Controller;
use hardtyz\health\models\Item;


class DefaultController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $arrItems = $this->module->check;
        $items =[];
        foreach ($arrItems as $arrItem) {
            $item= new Item;
            $item->setAttributes($arrItem,false);
            $items[] = $item;
        }
        return $this->render('index',array( 'items' => $items));
    }


}