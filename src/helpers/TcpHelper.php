<?php


namespace hardtyz\health\helpers;

class TcpHelper {


    static function check(string $host, int $port) {
        $hostip = @gethostbyname($host);
        if (!$x = @fsockopen($hostip, $port, $errno, $errstr, 5)) // attempt to connect
        {

            return false;
        }
        else
        {
            if ($x)
            {
                @fclose($x); //close connection
            }
            return true;
        }
    }

}