# yii2-health

## 1. Instalation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

and add next lines to `composer.json`
```json
    "repositories": [
        {
            "url": "https://bitbucket.org/HardTYZ/yii2-health",
            "type": "git"
        }
    ]
```

and add line to require section of `composer.json`

```json
"hardtyz/yii2-health": "*"
```

## Usage

##### Example configuration

Just simply add to your config in modules, you need to define your checks in "check" array.

```php
...
   'modules'   => [
        'health' => [
            'class'  => 'hardtyz\health\Module',
            'check' => [
                "TcpCheck" => [
                    "type" => "tcp",                    // type tcp
                    "host" => "localhost",              // tcp host or ip adress
                    "port" => 80,                       // tcp port
                    "name" => "TcpCheck"                // service name
                ],
                "HttpCheck" => [
                    "type" => "http",                   // type http
                    "host" => "http://localhost",       // http or https host
                    "name" => "HttpCheck"               // service name
                ],
                "DataBaseCheck" => [
                    "type" => "db",                     // type database
                    "db" => "db",                       // name of database profile
                    "name" => "DB"                      // service name
                ],
            ],
        ],
    ],
...
```